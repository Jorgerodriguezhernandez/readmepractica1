# TAREA 4 : GESTIÓN DE RAMAS

## Ejercicio 1:

Crearemos la rama bibliografia con el comando:

**git branch bibliografia** 

Para ver las ramas creadas en el repositorio usaremos el comando 

**git branch -av**

![Imagen_1](./imagenes/Ejercicio1P1.png "Imagen1")

---

## Ejercicio 2:

Creamos capitulo4.html con esto en su interior :

![Imagen_2](./imagenes/Ejercicio2P1.png "Imagen2")

Aplicaremos los cambios subiendolos al staging area con el comando **git add .** y realizaremos un commit con **git commit -m  _"Contenido del commit"_** , ahora mostraremos el historial con **git log --graph --all --online**

![Imagen_3](./imagenes/Ejercicio2P2.png "Imagen3")

---

## Ejercicio 3:

Cambiaremos de rama a bibliografia con el comando
**git checkout bibliografia** y con touch crearemos el archivo, en mi caso abrire visual studio code para hacerlo mas comodamente

![Imagen_4](./imagenes/Ejercicio2P1.png "Imagen4")

Ahora dejaremos el archivo de la siguiente manera

![Imagen_5](./imagenes/Ejercicio3P2.png "Imagen5")

Añadiremos los cambios al Staging Area con **git add .** , Haremos un commit con **git commit -m  _"Contenido del commit"_** y acabaremos mostrando el historial del repositorio con **git log --graph --all --online**

![Imagen_6](./imagenes/Ejercicio3P3.png "Imagen6")

---

## Ejercicio 4

Ahora fusionaremos la rama bibliografia con la rama master, para hacerlo debemos situarnos en master con el comando **git checkout master** y fusionaremos con el comando **git merge bibliografia** y mostraremos el historial con el comando **git log --graph --all --oneline**

![Imagen_7](./imagenes/Ejercicio4P1.png "Imagen7")

Ahora eliminaremos la rama bibliografia con el comando **git branch -d bibliografia**

![Imagen_8](./imagenes/Ejercicio4P2.png "Imagen8")

## Ejercicio 5

Ahora crearemos la rama bibliografia con el comando **git branch bibliografia** y nos situaremos en ella con el comando **git checkout bibliografia**

![Imagen_9](./imagenes/Ejercicio5P1.png "Imagen9")

Creamos el fichero bibliografia.html y lo dejamos con el siguiente contenido:

![Imagen_10](./imagenes/Ejercicio5P2.png "Imagen10")

Añadiremos los cambios al Staging Area con el comando **git add .** y haremos un commit con el comando **git commit -m  _"Contenido del commit"_**, Nos situaremos en la rama master con el comando **git checkout master**, añadiremos los cambios al Staging Area y haremos otro commit con **git commit -m  _"Contenido del commit"_**, ahora desde Master haremos un merge a bibliografia con el siguiente comando **git merge bibliografia**


![Imagen_11](./imagenes/Ejercicio5P3.png "Imagen11")

Ahora para terminar subiremos los cambios al Staging Area con el comando **git add .**, Despues de eso haremos un commit con el siguiente comando **git commit -m  _"Contenido del commit"_** y mostraremos el historial del repositorio

![Imagen_12](./imagenes/Ejercicio5P4.png "Imagen12")

---